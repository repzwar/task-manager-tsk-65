package ru.pisarev.tm;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pisarev.tm.component.Bootstrap;
import ru.pisarev.tm.config.DatabaseConfig;

public class Application {

    public static void main(final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.initApplication();
        bootstrap.initJMSBroker();
        bootstrap.start(args);
    }

}