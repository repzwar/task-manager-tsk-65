package ru.pisarev.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.pisarev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@Repository
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        save(new Task("Alpha"));
        save(new Task("Betta"));
        save(new Task("Gamma", "Epsilon"));
    }

    public void create() {
        save(new Task("Task" + System.currentTimeMillis()));
    }

    public void save(@NotNull Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull String id) {
        return tasks.get(id);
    }

    public void removeById(@Nullable String id) {
        tasks.remove(id);
    }

    public void saveAll(@NotNull List<Task> list) {
        tasks.putAll(list.stream().collect(Collectors.toMap(Task::getId, Function.identity())));
    }

    public void removeAll() {
        tasks.clear();
    }

}
