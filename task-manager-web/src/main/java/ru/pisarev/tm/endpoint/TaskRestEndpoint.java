package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.ITaskRestEndpoint;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

import static ru.pisarev.tm.constant.SiteConst.tasks_address;

@RestController
@RequestMapping(tasks_address)
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/saveAll")
    public List<Task> saveAll(@RequestBody List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        repository.removeAll();
    }

}
