package ru.pisarev.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.pisarev.tm.model.Project;

import java.util.List;

public interface IProjectRestEndpoint {
    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") String id);

    @PostMapping("/create")
    Project create(@RequestBody Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody List<Project> projects);

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody List<Project> projects);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();
}
