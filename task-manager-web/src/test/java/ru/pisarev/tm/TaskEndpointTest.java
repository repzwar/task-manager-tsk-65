package ru.pisarev.tm;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.client.TaskRestEndpointClient;
import ru.pisarev.tm.marker.WebCategory;
import ru.pisarev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskEndpointTest {

    final Task task = new Task("test");

    final Task task2 = new Task("test2");

    final TaskRestEndpointClient endpoint = new TaskRestEndpointClient();

    @BeforeClass
    public static void beforeClass() {
        new TaskRestEndpointClient().deleteAll();
    }

    @Before
    public void before() {
        endpoint.create(task);
    }

    @After
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(task.getName(), endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("updated");
        endpoint.save(updatedTask);
        Assert.assertEquals("updated", endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(task.getId());
        Assert.assertNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(task2);
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("updated1");
        final Task updatedTask2 = endpoint.find(task2.getId());
        updatedTask2.setName("updated2");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("updated1", endpoint.find(task.getId()).getName());
        Assert.assertEquals("updated2", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(task.getId());
        List<Task> list = new ArrayList<>();
        list.add(task);
        list.add(task2);
        endpoint.createAll(list);
        Assert.assertEquals("test", endpoint.find(task.getId()).getName());
        Assert.assertEquals("test2", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertEquals(0, endpoint.findAll().size());
    }


}
